//
// Created by kristian on 10/04/2020.
//


#import <iostream>
#import "Deck.h"
using namespace std;
Deck::Deck(){
    this->populateDeck();
}
void Deck::populateDeck(){
    for (int i = 0 ; i < 40; i++){
        cards.push_back(new Card(i));
    };
}
vector <Card*> Deck::getCards(){
    return cards;
}

Card*Deck::getRandomCard() {
    srand(time(NULL));
    int index =rand() % cards.size();
    Card *card = cards[index];
    cards.erase(cards.begin() + index);
    return card;
}
