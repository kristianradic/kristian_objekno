//
// Created by kristian on 10/04/2020.
//
#ifndef VJEZBA_GAME_H
#define VJEZBA_GAME_H

#include <vector>
#import <ctime>
#include "Card.h"
using namespace std;
class Deck {
private:
    vector <Card*> cards;
public:
    Deck();
    void populateDeck();
    vector <Card*> getCards();
    Card* getRandomCard();
};


#endif //BLACKJACK_DECK_H