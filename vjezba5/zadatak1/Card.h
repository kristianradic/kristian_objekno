//
// Created by kristian on 15/04/2020.
//

#ifndef VJEZBA_CARD_H
#define VJEZBA_CARD_H


#include <string>
#include <iostream>
class Card{
private:
    int cardWeight;
    int cardType;
    int id;
public:
    void addAuthor(const string, const string);
	void changeAuthor(const string, const string);
	void setTitle(const string);
	void setPublicationYear(const int);
	vector<Author> getAuthors() const;
	string getTitle() const;
	int getPublicationYear() const;
	void printAuthors() const;
};

#endif //VJEZBA_CARD_H
