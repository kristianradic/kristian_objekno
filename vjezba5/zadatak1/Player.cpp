//
// Created by kristian on 23/04/2020.
//

#import <iostream>
#include "Player.h"


Player::Player(){
    score = 0;
}


void Player::printCardsInHand(){
    for (Card *card : cardsInHand) {
        card->printCard() ;
    }
}


void Player::addCardToHand(Card *card){
    cardsInHand.push_back(card);
}

void Player::setName() {
    cout << "Unesite ime igrača" << endl;
    cin >>  playerName;
}

string Player::getName() {
    return playerName;
}

void Player::hasAkuza() {
    int asevi = 0;
    int duje = 0;
    int trice = 0;
    for (Card *card : cardsInHand) {
        if (card->getCardWeight() == 'A'){
            asevi++;
        }
        if (card->getCardWeight() == '2'){
            duje++;
        }
        if (card->getCardWeight() == '3'){
            trice++;
        }
    }

    if (asevi == 3) {
        setScore(3);
        cout << endl << "Igrač " << playerName  << " ima tri asa" << endl;
    }
    if (asevi == 4) {
        setScore(4);
        cout << endl << "Igrač " << playerName  << " ima 4 asa" << endl;
    }
    if (duje == 3) {
        setScore(3);
        cout << endl << "Igrač " << playerName  << " ima tri duje" << endl;
    }
    if (duje == 4) {
        setScore(4);
        cout << endl << "Igrač " << playerName  << " ima 4 duje" << endl;
    }
    if (trice == 3) {
        setScore(3);
        cout << endl << "Igrač " << playerName  << " ima tri trice" << endl;
    }
    if (trice == 4) {
        setScore(4);
        cout << endl << "Igrač " << playerName  << " ima 4 trice" << endl;
    }

    if (asevi < 3 && duje < 3 && trice < 3 ){
        cout << endl << "Igrač " << playerName << " nema akužu";
    }
}

void Player::setScore(int num) {
    score += num;
}


int Player::getScore() {
    return score;
}

