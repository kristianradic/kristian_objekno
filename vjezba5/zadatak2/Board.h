//
// Created by kristian on 22/05/2020.
//

#ifndef UNTITLED_BOARD_H
#define UNTITLED_BOARD_H


#pragma once
#include <iostream>
#include <algorithm>
#include <math.h>       /* rint */

#include "Point.h"

using namespace std;

enum angles { verthor, diagonal };
enum directions { updown, leftright, brtl, bltr }; //brtl(bottom-right top-left), bltr(bottom-left top-right)

class Board : public Point
{
private:
    void draw(int, int, int, char, const directions, const angles);
    int to_int(double x) { return int(rint(x)); }

public:
    char** board;
    unsigned int rows, columns;
    char frameSymbol;

    Board(unsigned int r = 8, unsigned int c = 8, char fs = 'x');

    void draw_char(int x, int y, char ch) { board[x][y] = ch; }
    void draw_up_line(Point, char);
    void draw_line(Point, Point, char);
    void display();

    ~Board();
};


#endif //UNTITLED_BOARD_H

