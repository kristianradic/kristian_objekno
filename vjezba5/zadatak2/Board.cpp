//
// Created by kristian on 21/05/2020.
//

#include "Board.h"

Board::Board(unsigned int r, unsigned int c, char fs)
{
    rows = r;
    columns = c;
    frameSymbol = fs;

    board = new char* [r];
    for (unsigned int i = 0; i < r; ++i) { board[i] = new char[c]; }

    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < columns; ++j) {
            board[i][j] = (i == 0 || i == rows - 1 || j == 0 || j == columns - 1) ? frameSymbol : ' ';
        }
    }
}

void Board::draw_up_line(Point p, char ch)
{
    int x = to_int(p.getX()), y = to_int(p.getY());

    while (x != 0) {
        draw_char(x, y, ch);
        --x;
    }
}

void Board::draw_line(Point p1, Point p2, char ch)
{
    int x1 = to_int(p1.getX()), y1 = to_int(p1.getY()), x2 = to_int(p2.getX()), y2 = to_int(p2.getY());

    if (y1 == y2) {
        draw(x1, y1, x2, ch, updown, verthor);
    }
    else if (x1 == x2) {
        draw(y1, x1, y2, ch, leftright, verthor);
    }
    else {
        directions dir;
        if ((x1 > x2 && y1 > y2) || (x1 < x2 && y1 < y2)) {
            dir = brtl;
        }
        else {
            dir = bltr;
        }
        draw(x1, y1, x2, ch, dir, diagonal);
    }
}


void Board::draw(int c1, int c1xy, int c2, char ch, const directions dir, const angles angl)
{
    if (angl == verthor) {
        while (c1 != c2) {
            (dir == updown) ? draw_char(c1, c1xy, ch) : draw_char(c1xy, c1, ch);
            (c1 > c2) ? --c1 : ++c1;
        }
        (dir == updown) ? draw_char(c1, c1xy, ch) : draw_char(c1xy, c1, ch);
    }
    else {
        while (c1 != c2) {
            draw_char(c1, c1xy, ch);
            if (dir == brtl) {
                (c1 > c2) ? --c1xy : ++c1xy;
            }
            else {
                (c1 > c2) ? ++c1xy : --c1xy;
            }
            (c1 > c2) ? --c1 : ++c1;
        }
        draw_char(c1, c1xy, ch);
    }
}

void Board::display()
{
    for (unsigned int i = 0; i < rows; ++i) {
        for (unsigned int j = 0; j < columns - 1; ++j) {
            cout << board[i][j] << " ";
        }
        cout << board[i][columns - 1] << endl;
    }
}

Board::~Board()
{
    for (unsigned int i = 0; i < rows; ++i) {
        delete[] board[i];
    }
    delete[] board;
}

