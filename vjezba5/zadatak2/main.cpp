#include <iostream>

#include "Board.h"

int main(){
    Board b(20, 10, 'o');
    Point p1(2, 2), p2(8, 8), p3(2, 8), p4(16, 8), p5(7, 1);

    b.draw_line(p1, p2, 'x');
    b.draw_line(p3, p4, 'x');
    b.draw_up_line(p5, 'x');

    b.display();

    return 0;
}

