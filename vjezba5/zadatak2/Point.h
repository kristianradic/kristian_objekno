//
// Created by kristian on 22/05/2020.
//

#ifndef UNTITLED_POINT_H
#define UNTITLED_POINT_H

#pragma once

class Point
{
private:
    double x, y;
public:
    Point(double x = 0, double y = 0) { this->x = x; this->y = y; }

    void set_XY(double x, double y) { this->x = x; this->y = y; }
    void setX(double x) { this->x = x; }
    double getX() const { return x; }
    void setY(double y) { this->y = y; }
    double getY() { return y; }
};


#endif //UNTITLED_POINT_H
