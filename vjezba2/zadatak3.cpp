#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

vector<int> noviVektor(){
    vector<int> veky;
	int element;
    int velicina = 4;
    cout << "Unesite vektore" << endl;
    cout << " " << endl;
	for (int i = 0; i < velicina; i++) {
		cout << "Element vektora: ";
		cin >> element;
		veky.push_back(element);
	}
	return veky;
}

void sumirajVektore(vector<int> veky, vector <int>& sumaVektora){
    int i = 0;
    int j= veky.size()-1;
    while (i < j){
        sumaVektora.push_back(veky[i]+veky[j]);
        i++;
        j--;
    }
    cout << "odradia san ovo" << endl;
}

void printajMinMax(vector<int> veky){
	sort(veky.begin(), veky.end());

	cout << "Min. element: " << veky.front() << endl;
	cout << "Max. element: " << veky.back() << endl;
}

int main(){
	vector<int> veky = noviVektor();
    vector <int> sumaVektora;
	sumirajVektore(veky, sumaVektora);
	printajMinMax(sumaVektora);
	return 0;
}

