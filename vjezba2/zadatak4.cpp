#include <iostream>
using namespace std;


/* 4. Napisati funkciju koja vraća niz int vrijednosti veličine n u kojem je svaki
element zbroj svoja dva prethodnika. Prvi i drugi element u nizu su jedinice.
U main funkciji ispisati dobiveni niz i osloboditi memoriju. */

int* goldenCut(int velicina){
    
	int* niz = new int[velicina];
	int i = 0;
	while (i < velicina){
		if ( i == 0 || i == 1) {
			niz[i] = 1;
		}
		else {
			niz[i] = niz[i - 2] + niz[i - 1];
		}
		i++;
	}

	return niz;
}


int main() {
    cout << "Unesite broj elemenata liste" << endl;
    int velicina;
    cin >> velicina;
	int* niz = goldenCut(velicina);

	for (int i = 0; i < velicina; i++) {
		cout << niz[i] << " ";
	}

	return 0;
}