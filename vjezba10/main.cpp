#include <iostream>
#include <fstream>
#include <iterator>
#include <vector>
#include <algorithm>

using namespace std;


bool smallerThen(int i){
    return i < 300;
}

bool largerThen(int i){
    return i > 500;
}

int sortNumbers(int i, int j)
{
    return i > j;
}

int main(){
    vector<int> vectorNumbers;
    try {
        ifstream f("numbers.txt");
        if (!f)
            throw 1;
    }
    catch (int){
        cout << "Datoteka ne postoji";
    }

    ifstream f("numbers.txt");
    istream_iterator<int> is(f), eos;
    copy(is, eos, back_inserter(vectorNumbers));
    ostream_iterator<int> os(cout, ",");


    cout << "Brojevi veci od 500 " << count_if(vectorNumbers.begin(), vectorNumbers.end(), largerThen) << endl;
    pair<vector<int>::iterator, vector<int>::iterator> result = minmax_element(vectorNumbers.begin(), vectorNumbers.end());
    cout << "Najmani broj " << *result.first << endl;
    cout << "Najveci broj " << *result.second << endl;
    cout << "Brojevi veci od 300" << endl;
    vectorNumbers.erase(remove_if(vectorNumbers.begin(), vectorNumbers.end(), smallerThen),vectorNumbers.end());
    copy(vectorNumbers.begin(), vectorNumbers.end(), os);
    sort(vectorNumbers.begin(), vectorNumbers.end(), sortNumbers);
    cout << endl;
    cout << "Sortirani brojevi" << endl;
    copy(vectorNumbers.begin(), vectorNumbers.end(), os);
}
