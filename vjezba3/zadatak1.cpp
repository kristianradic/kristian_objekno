#include <iostream>
using namespace std;
#include <iostream>
#include <array>

/* 1. Napisati funkciju koja računa najveći i najmanji broj u nizu od n prirodnih brojeva.
 *    Funkcija vraća tražene brojeve pomoću referenci. */

void calcMaks(int *niz, int size, int &min, int &maks) {
    min = niz[0];
    maks = niz[0];
    for (int i = 1; i < size; ++i) {
        if (niz[i] > maks){
            maks = niz[i]; 
        }
        if (niz[i] < min){
            min = niz[i]; 
        }
    }
    cout << "Poslje funkcije " << min <<" " << maks << endl;
}

int main() {
    int lista[5] = {1,2,3,21,7};
    int size = sizeof(lista) / sizeof(lista[0]);
    int min = 0;
    int maks = 0;
    cout <<"Prije funkcije " << min << " " << maks << endl;
    calcMaks(lista, size, min, maks);
    return 0;
}