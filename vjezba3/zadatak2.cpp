#include <iostream>
#include <algorithm>
#include <string>
using namespace std;

string reverse_string(string recenica){
    reverse(recenica.begin(), recenica.end());  //acinecer ej ovo 
	
    string rijec = "", nova = "";
    
    for (int i= 0; i < recenica.size(); i++){
        if (recenica[i] == ' '){
            reverse(rijec.begin(), rijec.end());
            nova += rijec + " ";
            rijec = "";
        }
        else{
            rijec += recenica[i];
        }
    }
    reverse(rijec.begin(), rijec.end());
    nova += rijec + " ";
    
	return nova;
}

int main(){
	string str = "Ovo je moja recenica prije reversa";
	cout << str << endl;
	
	str = reverse_string(str);
	cout << "Nakon: " << str << endl;
	
	return 0;

}