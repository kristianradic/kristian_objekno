//
// Created by kristian on 05/06/2020.
//

#ifndef VJEZBA9_HEADER_H
#define VJEZBA9_HEADER_H
template <typename T>
void sortirajNiz(T* el, int counter) {
    for (int i = 1; i < counter; ++i) {
        for (int j = counter - 1; j >= i; --j) {
            if (el[j - 1] > el[j]) {
                swap(el[j - 1], el[j]);
            }
        }
    }
}
template <typename T>
void sortirajNiz(char* el, int counter) {
    for (int i = 1; i < counter; ++i) {
        for (int j = counter - 1; j >= i; --j) {
            if (el[j - 1] > el[j] ) {
                swap(el[j - 1], el[j]);
            }
        }
    }
}
#endif //VJEZBA9_HEADER_H
