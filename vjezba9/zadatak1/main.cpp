#include <iostream>
using namespace  std;
#include "header.h"

int main() {
    int intigers[10] = { 2,1,3,4,6,2,7,7,9,5};
    float floats[3] = { 2.9,2.2,1.3 };
    double doubleF[5] = { 2.2,3.1,5.4,1.6,5.6 };
    char chars[8] = { 'a','c','d','b','e','f','k','j' };
    sortirajNiz(intigers, 10);
    for (int i = 0; i<10; i++) {
        cout << intigers[i] << " ";
    }
    cout << endl << "floatovi" << endl;
    sortirajNiz(floats, 3);
    for (int i = 0; i < 3; i++) {
        cout << floats[i] << " ";
    }
    sortirajNiz(doubleF, 5);
    cout << endl << "double floatovi" << endl;
    for (int i = 0; i < 5; i++) {
        cout << doubleF[i] << " ";
    }
    sortirajNiz(chars, 8);
    cout << endl << "charovi" << endl;
    for (int i = 0; i < 8; i++) {
        cout << chars[i] << " ";
    }
    return 0;
}