#include <iostream>

#include "Stack.cpp"
#include <type_traits>
using namespace  std;

int main() {
    Stack<int> stog(10);
    stog.print();
    stog.push(5);
    stog.push(6);
    stog.push(7);
    stog.print();
    stog.pop();
    stog.pop();
    stog.pop();
    stog.print();
    stog.pop();
    if (stog.is_empty()) {
        cout << "Stog je prazan!" << endl;
    }
    else {
        cout << "Stog nije prazan!" << endl;
    }
    return 0;
}