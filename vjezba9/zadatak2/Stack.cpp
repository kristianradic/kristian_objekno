//
// Created by kristian on 05/06/2020.
//

#include "Stack.h"
using namespace std;

template<typename T> Stack<T>::Stack(int broj) {
    niz = new T[broj];
    elementi = broj;
    top = -1;
}
template<typename T> Stack<T>::~Stack() {
    delete[] niz;
}
template<typename T> void Stack<T>::push(T clan) {
    niz[++top] = clan;
}
template<typename T> T Stack<T>::pop() {
    if (is_empty()) {
        cout << "Ne možete skidati element s stoga, jer stog je prazan." << endl;
        return -1;
    }
    else {
        return niz[top--];
    }
}
template<typename T> bool Stack<T>::is_empty()  {
    return (top == -1);
}

template<typename T> void Stack<T>::print()  {
    if (is_empty()) {
        cout << "Stog je prazan" << endl;
    }
    else {
        for (int i = 0; i <= top; ++i) {
            cout << " "<< niz[i];
        }
        cout << endl;
    }
}