//
// Created by kristian on 05/06/2020.
//

#ifndef DEVETADRUGI_STACK_H
#define DEVETADRUGI_STACK_H

#include <iostream>
#include <algorithm>
#include <iterator>

template<typename T>

class Stack
{
    T* niz;
    int elementi;
    int top;
public:
    Stack(int);
    ~Stack();
    void push(T);
    T pop();
    bool is_empty();
    void print();
};



#endif //DEVETADRUGI_STACK_H
