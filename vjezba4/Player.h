//
// Created by kristian on 23/04/2020.
//

#ifndef VJEZBA_PLAYER_H
#define VJEZBA_PLAYER_H


#import "Card.h"
#import <vector>
#include "string"
using namespace std;

class Player{
private:
    vector <Card*> cardsInHand;
    int score;
    string playerName;
public:
    Player();
    void setName();
    string getName();
    void printCardsInHand();
    void addCardToHand(Card *card);
    void hasAkuza();
    void setScore(int num);
    int getScore();


};


#endif //VJEZBA_PLAYER_H
