//
// Created by kristian on 15/04/2020.
//

#ifndef VJEZBA_CARD_H
#define VJEZBA_CARD_H


#include <string>
#include <iostream>
class Card{
private:
    int cardWeight;
    int cardType;
    int id;
public:
    explicit Card(int id);
    char getCardWeight();
    char getCardType();
    void printCard();
};

#endif //VJEZBA_CARD_H
