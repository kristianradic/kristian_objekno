//
// Created by kristian on 10/04/2020.
//


#include "Game.h"
#import "Card.h"
#import "Deck.h"
#import "Player.h"
#include <iostream>
using namespace std;


Game::Game(){
}

void Game::setGame(){
    vector <Card*> cards = deck.getCards();
    cout << "Ovo je deck " << endl;
    for (Card *card: cards){
        cout << card->getCardWeight() << card->getCardType() << " ";
    }
    cout << endl;
    Player player1;
    Player player2;
    int numberOfPlayers;
    cout << "Unesite broj igrača (2 ili 4)" << endl;
    cin >> numberOfPlayers;
    while(numberOfPlayers > 0){
        if(numberOfPlayers == 2){
            cout << endl;
            player1.setName();
            player2.setName();
            for (int i = 0; i < 10; i++) {
                player1.addCardToHand(deck.getRandomCard());
                player2.addCardToHand(deck.getRandomCard());
            }
            cout << "Karte svih igrača" << endl;

            cout << player1.getName() << endl;
            player1.printCardsInHand();
            cout << endl;
            cout << player2.getName() << endl;
            player2.printCardsInHand();


            player1.hasAkuza();
            player2.hasAkuza();

            cout << endl << player1.getScore();
            cout << endl << player2.getScore();
            break;
        }
        if(numberOfPlayers == 4){
            cout << "Zbog korone nije dozvoljeno igrat u četvoro!" << endl;
            break;
        }
        else{
            cout << "Ne dozvoljen broj igrača" << endl;
            cout << "Unesite broj igrača (2 ili 4)"<< endl;
            cin >> numberOfPlayers;
        }
    };
}
