//
// Created by kristian on 04/06/2020.
//

#ifndef Money


#pragma once

#include <iostream>
using namespace std;

class Money {
public:
    int kn = 0;
    int lp = 0;
public:
    Money(int, int);
    Money(int);
    ~Money();
    Money operator+(const Money& varijabla);
    Money operator-(const Money& varijabla);
    Money& operator+=(const Money& varijabla);
    Money& operator-=(const Money& varijabla);
    friend ostream& operator<< (ostream& os, const Money& p);
    operator double();

};

#endif