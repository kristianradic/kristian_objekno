//
// Created by kristian on 04/06/2020.
//

#include "Money.h"

Money::Money(int kn, int lp) {
    this -> kn = 0;
    this -> lp = 0;
}

Money::Money(int kn) {
    this -> kn = 0;
}


Money::~Money(){

}

Money Money::operator+(const Money& varijabla) {
    int lipe = this->lp + varijabla.lp;
    int kune = this->kn + varijabla.kn;
    if (lipe >= 100) {
        kune++;
        lipe -= 100;
    }
    cout << kune << lipe << endl;
    return Money(kune, lipe);
}
Money Money::operator-(const Money& varijabla) {
    int lipe = this->lp-varijabla.lp;
    int kune = this->kn-varijabla.kn;
    if (lipe < 0) {
        kune--;
        lipe += 100;
    }
    return Money(kune, lipe);
}
Money& Money::operator+=(const Money& varijabla) {
    this->lp += varijabla.lp;
    this->kn += varijabla.kn;
    if (this->lp >= 100) {
        this->kn++;
        this->lp -= 100;
    }
    return *this;
}
Money& Money::operator-=(const Money& varijabla) {
    this->lp -= varijabla.lp;
    this->kn -= varijabla.kn;
    if (this->lp < 0) {
        this->kn--;
        this->lp += 100;
    }
    return *this;
}

ostream &operator<<(ostream &os, const Money &pare) {
    os << pare.kn << " kuna, " <<  "i " << pare.lp << " lipa.";
    return os;
}

Money::operator double() {
    double tip = this->kn + this->lp / 100.0000;
    return tip;
}
