//
// Created by kristian on 21/08/2020.
//


#import <iostream>
#import "Player.h"


Player::Player(){
}


void Player::printCardsInHand(){
    for (Card *card : cardsInHand) {
        card->printCard() ;
    }
}

int Player::getNumberOfCards(){
    return cardsInHand.size();
}

void Player::addCardToHand(Card *card){
    cardsInHand.push_back(card);
    score += card->getCardWeightValue();
}

int Player::getScore() {
    return score;
}

bool Player::isBusted() {
    if (score > 21) {
        return true;
    }

    return false;
}

bool Player::hasBlackjack() {
    return (score == 21);
}