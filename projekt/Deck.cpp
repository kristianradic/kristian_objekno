//
// Created by kristian on 21/08/2020.
//

#import <iostream>
#import "Deck.h"
using namespace std;


Deck::Deck(){
    this->populateDeck();
    cout << "Stvoren dek" << endl;
}

void Deck::populateDeck(){
    for (int i = 0 ; i < 52; i++){
        cards.push_back(new Card(i));
    };
}

vector <Card*> Deck::getCards(){
    return cards;
}

Card*Deck::getRandomCard() {
    int index =rand() % cards.size();
    Card *card = cards[index];
    cards.erase(cards.begin() + index);
    return card;
}


