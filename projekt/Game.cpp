//
// Created by kristian on 21/08/2020.
//

#include "Game.h"

#include <iostream>
using namespace std;
#import "Player.h"
#import "Card.h"
#import <string>

Game::Game(){
}

void Game::setGame(){
    vector <Card*> cards = deck.getCards();
    for (Card *card: cards) //za svaki objekt u vectoru, ce ga gledat da je tipa Card:
        cout << card->getCardWeight() << card->getCardType() << " ";
    cout << "" << endl;

    cout << "These are cards of a humanPlayer" << endl;
    playerHuman.addCardToHand(deck.getRandomCard());
    playerHuman.addCardToHand(deck.getRandomCard());
    playerHuman.printCardsInHand();
    cout << "These are cards of a computerPlayer" << endl;
    playerComputer.addCardToHand(deck.getRandomCard());
    //playerComputer.addCardToHand(deck.getRandomCard()); kompjuteru triba samo jedna karta za pocetak, ovako ne moran maskirat drugu nego kasnije samo dodan isti broj karata
    playerComputer.printCardsInHand();

}


void Game::startGame(){
    bool gameEnded = false;
    do {
        if (inputHitOrStay()) {
            cout << "Currently you are holding" << endl;
            playerHuman.addCardToHand(deck.getRandomCard());

            if (playerHuman.isBusted() || playerHuman.hasBlackjack()) {
                gameEnded = true;
            }

            playerHuman.printCardsInHand();

        } else {
            gameEnded = true;
            if (playerHuman.getNumberOfCards() != playerComputer.getNumberOfCards()) {
                while ((playerComputer.getScore() < 17) && (playerComputer.getNumberOfCards() < playerHuman.getNumberOfCards())) {
                    playerComputer.addCardToHand(deck.getRandomCard());
                }
            }

            if ((playerHuman.getScore() >= 21) || playerHuman.getNumberOfCards() == playerComputer.getNumberOfCards()) {
                gameEnded = true;
            }
            if (playerHuman.getScore() > playerComputer.getScore()) {
                cout << "************************" << endl;
                cout << "Player wins!" << endl;
                cout << "************************" << endl;
            } else if (playerHuman.getScore() < playerComputer.getScore()) {
                cout << "************************" << endl;
                cout << "Computer wins!" << endl;
                cout << "************************" << endl;
            } else {
                cout << "Draw" << endl;
            }
        }

    } while (!gameEnded);

    if (playerHuman.isBusted()) {
        cout << "************************" << endl;
        cout << "Computer wins.!" << endl;
        cout << "************************" << endl;
    } else if (playerHuman.hasBlackjack()) {
        cout << "Player wins with a blackjack!" << endl;
    } else if (playerComputer.isBusted()) {
        cout << "************************" << endl;
        cout << "Player wins!" << endl;
        cout << "************************" << endl;
    } else if (playerComputer.hasBlackjack()) {
        cout << "Computer wins with a blackjack!" << endl;
    }
    cout << "---- Player hand was: ----" << endl;
    playerHuman.printCardsInHand();
    cout << "---- Score: " << playerHuman.getScore() << " ----"<< endl;

    cout << "---- Computer hand was: ----" << endl;
    playerComputer.printCardsInHand();
    cout << "---- Score: " << playerComputer.getScore() << " ----"<< endl;
    cout << "---------------------------------------" << endl;
}



bool Game::inputHitOrStay(){
    string input;
    do {
        cout << "Please enter 'hit' or 'stay'" << endl;
        cin >> input;
    } while ((input != "hit") && (input != "stay"));

    if (input == "hit") {
        return true;
    }

    return false;
}