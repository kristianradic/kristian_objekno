//
// Created by kristian on 21/08/2020.
//

#ifndef SEMINAR_CARD_H
#define SEMINAR_CARD_H

#include <string>
#include <iostream>
class Card{
private:
    int cardWeight;
    int cardType;
    int id;
public:
    explicit Card(int id);
    int getCardWeightValue();
    char getCardWeight();
    char getCardType();
    void printCard();


};


#endif //SEMINAR_CARD_H
