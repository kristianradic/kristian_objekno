#include <iostream>

#include "Game.h"

#import <string>


using namespace std;


int main() {
    srand((unsigned)time(NULL)) ;
    Game game;
    game.setGame();
    game.startGame();
    game.~Game();
    string playAgain = "yes";
    while (playAgain == "yes") {
        cout << "Do you want to play again?" << endl;
        cout << "Enter 'yes' or 'no'"  << endl;
        cin >>  playAgain;
        if (playAgain != "yes"){
            cout << "Good luck and have fun with your life." << endl;
            break;
        }
        Game game;
        game.setGame();
        game.startGame();
        game.~Game();
    }

    return 0;
}

/*while (playAgain == "yes") {
int gameNumber = 0;
gameNumber++;
string gameName = "0" + gameNumber;
cout << "Do you want to play again?" << endl;
cout << "Enter 'yes' or 'no'"  << endl;
cin >>  playAgain;
if (playAgain == "no"){
cout << "Good luck and have fun with your life." << endl;
break;
}
Game gameName;
game.setGame();
game.startGame();
game.~Game();
}*/

