//
// Created by kristian on 21/08/2020.
//

#ifndef SEMINAR_COMPUTERPLAYER_H
#define SEMINAR_COMPUTERPLAYER_H


#include "Player.h"

class ComputerPlayer : public Player {
public:
    ComputerPlayer();
};

#endif //SEMINAR_COMPUTERPLAYER_H
