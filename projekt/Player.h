//
// Created by kristian on 21/08/2020.
//

#ifndef SEMINAR_PLAYER_H
#define SEMINAR_PLAYER_H


#import "Card.h"
#import <vector>
#include "string"
using namespace std;

class Player{
private:
    vector <Card*> cardsInHand;
    int score;
protected:
    string playerName;
public:
    Player();
    void printCardsInHand();
    int getNumberOfCards();
    void addCardToHand(Card *card);
    int getScore();
    bool isBusted();
    bool hasBlackjack();
};

#endif //SEMINAR_PLAYER_H
