//
// Created by kristian on 21/08/2020.
//

#ifndef SEMINAR_DECK_H
#define SEMINAR_DECK_H


#include <vector>
#import <ctime>
#include "Card.h"
using namespace std;
class Deck {
private:
    vector <Card*> cards;
public:
    Deck();
    void populateDeck();
    vector <Card*> getCards();
    Card* getRandomCard();

};

#endif //SEMINAR_DECK_H
