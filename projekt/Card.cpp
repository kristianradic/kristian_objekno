//
// Created by kristian on 21/08/2020.
//


#include "Card.h"
using namespace std;

Card::Card(int id){
    this->id = id;
    this->cardWeight = (id % 13);
    this->cardType = (id / 13);
}

int Card::getCardWeightValue() {
    bool softHand = true; //moga bi napravit da ako mu zbroj vrjednosti karata u prvom dealtu je manji od 10, da as vridi 11
    if (cardWeight >= 10 ){
        return 10;
    }
    if (cardWeight == 0){
        return 11;
    }
    return cardWeight + 1;

}

char Card::getCardWeight(){
    if (cardWeight != 0 && cardWeight < 9){
        return (cardWeight + 1) + '0';
    }
    if (cardWeight == 9 ){
        return 'T';
    }
    if (cardWeight == 10){
        return 'J';
    }
    if (cardWeight == 11){
        return 'Q';
    }
    if (cardWeight == 12){
        return 'K';
    }
    return 'A';
}

char Card::getCardType(){
    if (cardType == 0){
        return 'H';
    }
    if (cardType == 1){
        return 'S';
    }
    if (cardType == 2){
        return 'C';
    }
    return 'D';
}

void Card::printCard() {
    cout << getCardWeight() << getCardType() << endl;
}

