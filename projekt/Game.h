//
// Created by kristian on 21/08/2020.
//

#ifndef SEMINAR_GAME_H
#define SEMINAR_GAME_H



#include "Deck.h"
#include "Player.h"
#include "HumanPlayer.h"
#include "ComputerPlayer.h"
#include <string>

class Game {
protected:
    Deck deck;
    HumanPlayer playerHuman;
    ComputerPlayer playerComputer;
public:
    Game();
    void setGame();
    void startGame();
    bool inputHitOrStay();
};



#endif //SEMINAR_GAME_H
