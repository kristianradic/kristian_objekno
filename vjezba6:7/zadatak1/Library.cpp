//
// Created by kristian on 26/05/2020.
//

#include "Library.h"

void Library::addBook(Book* book) {
	books.push_back(book);
}

vector<Book*> Library::getBooks() {
	return books;
}

void Library::printBooks()
{
	for (vector<Book*>::const_iterator it = books.begin(); it != books.end(); ++it) {
		cout << (*it)->getAuthor() << ", " << (*it)->getTitle() << ", " << (*it)->getYear() << endl;
	}
}

vector<string> Library::getAuthorTitles(string author) {
	vector<string> titles;

	for (vector<Book*>::const_iterator it = books.begin(); it != books.end(); ++it) {
		if ((*it)->getAuthor().find(author) != string::npos) {
			titles.push_back((*it)->getTitle());
		}
	}

	return titles;
}

float Library::getAuthorBooksSize(string author) {
	float total = 0;

	for (vector<Book*>::const_iterator it = books.begin(); it != books.end(); ++it) {
		if ((*it)->getAuthor().find(author) != string::npos) {
			total += (*it)->getSizeMB();
		}
	}
	return total;
}

vector<string> Library::getBooksWithTitle(string title) {
	vector<string> titles;

	for (vector<Book*>::const_iterator it = books.begin(); it != books.end(); ++it) {
		if ((*it)->getTitle().find(title) != string::npos) {
			titles.push_back((*it)->getTitle());
		}
	}
	return titles;
}
