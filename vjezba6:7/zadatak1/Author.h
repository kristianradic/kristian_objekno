//
// Created by kristian on 25/05/2020.
//

#ifndef SESTA_AUTHOR_H
#define SESTA_AUTHOR_H


#include <iostream>
#include <string>

using namespace std;


class Author
{
private:
    string fullName;
public:
    Author();
    void setFullname();
    string getFullName();
    ~Author() {};
};

#endif //SESTA_AUTHOR_H
