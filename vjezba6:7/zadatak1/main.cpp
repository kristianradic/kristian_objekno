#include "HardCopyBook.h"
#include "EBook.h"
#include "Library.h"

void print(vector<string> vect) {
	for (vector<string>::const_iterator it = vect.begin(); it != vect.end(); ++it) {
		cout << *it << endl;
	}
}

void main() {
	Library lib;

	HardCopyBook book("Aldous Huxley", "Brave New World", 2005, 286);
	Book* bPtr = &book;
	lib.addBook(bPtr);
	EBook book1("Shin Takahashi, Iroha Inoue", "The Manga Guide to Linear Algebra", 2003, "No_Starch_The_Manga_Guide_to_Linear_Algebra.pdf", 34.6);
	Book* bPtr1 = &book1;
	lib.addBook(bPtr1);
	HardCopyBook book2("Neal Stephenson", "Snow Crash", 2000, 476);
	Book* bPtr2 = &book2;
	lib.addBook(bPtr2);
	EBook book3("Allen Holub", "Enough Rope to Shoot Yourself in the Foot", 1995, "Enough_Rope_to_Shoot_Yourself_in_the_Foot.pdf ", 5);
	Book* bPtr3 = &book3;
	lib.addBook(bPtr3);
	EBook book4("Simson Garfinkel, Daniel Weise and Steven Strassmann", "The Unix-Haters Handbook", 1967, "nixhatershandbook.pdf", 3.5);
	Book* bPtr4 = &book4;
	lib.addBook(bPtr4);

	lib.printBooks();
	vector<string> authorTitles = lib.getAuthorTitles("Iroha");
	cout << endl << "Books from author: " << endl;
	print(authorTitles);
	float authorBookSize = lib.getAuthorBooksSize("Iroha");
	cout << "Size of books from author: " << authorBookSize << endl;
	vector<string> titlesWith = lib.getBooksWithTitle("in");
	cout << "Titles with word in it: " << endl;
	print(titlesWith);
}