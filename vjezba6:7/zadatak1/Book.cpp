//
// Created by kristian on 25/05/2020.
//

#include "Book.h"

Book::Book(string author, string title, int year) {
	this->author = author;
	this->title = title;
	this->year = year;
}

void Book::setAuthor(string author) {
	this->author = author;
}

string Book::getAuthor() {
	return author;
}

void Book::setTitle(string title) {
	this->title = title;
}

string Book::getTitle() {
	return title;
}

void Book::setYear(int year) {
	this->year = year;
}

int Book::getYear() {
	return year;
}

