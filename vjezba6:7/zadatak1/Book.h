//
// Created by kristian on 25/05/2020.
//
#pragma once

#include <iostream>
#include <string>

using namespace std;

class Book {
protected:
	string author;
	string title;
	int year;
public:
	Book(string = "", string = "", int = 0);
	void setAuthor(string);
	string getAuthor();
	void setTitle(string);
	string getTitle();
	void setYear(int);
	int getYear();
	virtual float getSizeMB() = 0;
	virtual ~Book() {};
};

