//
// Created by kristian on 25/05/2020.
//

#include "HardCopyBook.h"

HardCopyBook::HardCopyBook(string author, string title, int year, int numOfPages) : Book(author, title, year) {
	numberOfPages = numOfPages;
}

void HardCopyBook::setNumberOfPages(int num) {
	numberOfPages = num;
}
int HardCopyBook::getNumberOfPages() {
	return numberOfPages;
}

void HardCopyBook::printNumOfPages() {
	cout << "Broj stranica " << numberOfPages << endl;
}



