//
// Created by kristian on 26/05/2020.
//

#pragma once

#include <vector>
#include <iterator>

#include "Book.h"

class Library {
private:
	vector<Book*> books;
public:
	Library() {};
	void addBook(Book*); //dodaj knjigu
	vector <Book*> getBooks(); //dohvati knjige
	void printBooks();
	
	vector<string> getAuthorTitles(string); //dohvati sve naslove autora
	float getAuthorBooksSize(string); //vrati mi veli�inu u MB E-knjiga autora
	vector<string> getBooksWithTitle(string); //vektor naslova koji sadr�e "string"
	~Library() {};
};

