//
// Created by kristian on 25/05/2020.
//

#include "EBook.h"

EBook::EBook(string author, string title, int year, string fileName, float fileSize) : Book(author, title, year) {
	file = fileName;
	sizeMB = fileSize;
}

void EBook::setFileName(string fileName) {
	file = fileName;
}

string EBook::getFileName() {
	return file;
}

void EBook::printFileName() {
	cout << "File: " << file << endl;
}

void EBook::setSizeInMB(float size) {
	sizeMB = size;
}

float EBook::getSizeMB() {
	return sizeMB;
}

void EBook::printSizeInMB() {
	cout << "Veli�ina u megabajtima: " << sizeMB << endl;
}