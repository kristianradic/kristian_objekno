//
// Created by kristian on 25/05/2020.
//

#pragma once

#include "Book.h"

class EBook :  public Book {
protected:
	string file;
	float sizeMB;
public:
	EBook(string, string, int, string, float);
	void setFileName(string);
	string getFileName();
	void printFileName();
	void setSizeInMB(float);
	float getSizeMB() override;
	void printSizeInMB();
	~EBook() {};
};


