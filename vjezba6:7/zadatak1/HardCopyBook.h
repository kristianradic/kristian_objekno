//
// Created by kristian on 25/05/2020.
//

#pragma once

#include "Book.h"

class HardCopyBook : public Book {
private:
	int numberOfPages;
public:
	HardCopyBook(string author, string title, int year, int pages);
	void setNumberOfPages(int);
	int getNumberOfPages();
	void printNumOfPages();
	float getSizeMB() override { return 0; }
	~HardCopyBook() {};
};



